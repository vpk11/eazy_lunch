package de.clark.easylunch.utils

/**
 * @author vpk
 * Last Modified : MAY 20 2022
 */

class Constants {
    companion object {
        const val PRESS_AGAIN_TO_EXIST = "Press Again to Exit"
        const val EAZY_LUNCH = "https://google.com"
        const val LOADING = "Loading"
    }
}